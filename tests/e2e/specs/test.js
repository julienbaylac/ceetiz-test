describe("Just test the CRUD", () => {
  it("Display correctly home page", () => {
    cy.visit("/");

    cy.contains("h2", "Activités du moment");
    cy.get(".activity-card").should("have.length", 4);
  });

  it("visit details page of an activity", () => {
    cy.visit("/");

    cy.get("#get-details-button-5493458").click();
    cy.contains("Billet Disneyland Paris");
  });

  it("add an activity to cart", () => {
    cy.visit("/activity/5493458");

    cy.get("#date-input").type("2023-05-06");
    cy.get("#submit-button").click();
    cy.get("#cart-button").should("contain", "1");
  });

  it("remove an activity from the cart", () => {
    cy.visit("/activity/5493458");

    cy.get("#date-input").type("2023-05-06");
    cy.get("#submit-button").click();

    cy.get("#cart-button").click();

    cy.get("#remove-button").click();
    cy.get("div").should(
      "contain",
      "Aucune reservation n'a été ajoutée au panier"
    );
  });
});
