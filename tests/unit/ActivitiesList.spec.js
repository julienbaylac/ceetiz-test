import { mount, createLocalVue } from "@vue/test-utils";
import Vuex from "vuex";
import ActivitiesList from "@/components/ActivitiesList.vue";

const localVue = createLocalVue();
localVue.use(Vuex);

describe("ActivitiesList", () => {
  it("should render the list", () => {
    const wrapper = mount(ActivitiesList, {
      propsData: {
        title: "Activités du moment",
        activities: [
          {
            activity_code: 5493458,
            title: "Billet Disneyland Paris",
            description:
              "D'un côté, le classique parc Disneyland Paris regroupe tous vos personnages préférés dans les attactions mythiques telles que Peter Pan, Pirates des Caraïbes ou encore Pinocchio. Vous ferez le plein de sensations fortes à Space Mountain, le Train de la Mine ou Indiana Jones. Vous pourrez aussi vous balader sur Main Street USA, la rue principale qui regorge de boutiques souvenirs. C'est également là que passe la parade, un incontournable !",
            image_src:
              "https://media.ceetiz.com/activity/TIQGLO008/billet-disneyland-paris-ceetiz-6.jpg",
            age_prices: {
              child: 45,
              adult: 85,
            },
            days_off: ["lundi", "mardi", "mercredi", "jeudi", "vendredi"],
            with_reservation: true,
          },
        ],
      },
    });

    expect(wrapper.find("h2").text()).toMatch("Activités du moment");
    expect(wrapper.find(".v-card__title").text()).toMatch(
      "Billet Disneyland Paris"
    );
  });
});
