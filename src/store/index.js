import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import VueAxios from "vue-axios";

Vue.use(Vuex);
Vue.use(VueAxios, axios);

export default new Vuex.Store({
  state: {
    activities: [],
    cartList: [],
    alert: false,
  },

  getters: {
    activities: (state) => {
      return state.activities;
    },
    cartList: (state) => {
      return state.cartList;
    },
    alert: (state) => {
      return state.alert;
    },
  },

  mutations: {
    SET_ACTIVITIES(state, activities) {
      state.activities = activities;
    },
    ADD_CART_LIST(state, activity) {
      state.cartList.push(activity);
    },
    REMOVE_CART_LIST(state, code) {
      const activity = state.cartList.find(
        (activity) => activity.activity_code === code
      );
      const index = state.cartList.indexOf(activity);
      state.cartList.splice(index, 1);
    },
    DISPLAY_ALERT(state, alert) {
      state.alert = alert;

      setTimeout(function () {
        state.alert = false;
      }, 3000);
    },
  },

  actions: {
    //exemple of loading from api
    // loadActivities({ commit }) {
    //   axios
    //     .get("http://localhost:3000/activities")
    //     .then((response) => response.data)
    //     .then((activities) => {
    //       commit("SET_ACTIVITIES", activities);
    //     })
    //     .catch(function () {
    //       commit("DISPLAY_ALERT", {
    //         mainColor: "error",
    //         backgroundColor: "#ffd7d7",
    //         message: "Your activity list failed to load",
    //         icon: "mdi-alert-circle-outline",
    //       });
    //     });
    // },

    loadActivitiesWithoutAPI({ commit }) {
      const activities = [
        {
          activity_code: 5493458,
          title: "Billet Disneyland Paris",
          description:
            "D'un côté, le classique parc Disneyland Paris regroupe tous vos personnages préférés dans les attactions mythiques telles que Peter Pan, Pirates des Caraïbes ou encore Pinocchio. Vous ferez le plein de sensations fortes à Space Mountain, le Train de la Mine ou Indiana Jones. Vous pourrez aussi vous balader sur Main Street USA, la rue principale qui regorge de boutiques souvenirs. C'est également là que passe la parade, un incontournable !",
          image_src:
            "https://media.ceetiz.com/activity/TIQGLO008/billet-disneyland-paris-ceetiz-6.jpg",
          age_prices: {
            child: 45,
            adult: 85,
          },
          days_off: ["lundi", "mardi", "mercredi", "jeudi", "vendredi"],
          with_reservation: true,
        },
        {
          activity_code: 54323457,
          title: "Visite de Paris en bus",
          description:
            "Le bus vous emmènera à la découverte des lieux les plus emblématiques de la capitale : la Tour Eiffel, le Champ de Mars, l'Opéra - Galeries Lafayette, le Musée du Louvre, Notre-Dame-de-Paris, le Musée d'Orsay, les Champs Elysées - Etoile, le Grand Palais, le Trocadéro, et bien d’autres endroits tout aussi incontournables.",
          image_src:
            "https://media.ceetiz.com/activity/CARPAR001/pass-bus-imperial-paris-ceetiz.jpg",
          days_off: ["samedi", "dimanche"],
          age_prices: {
            child: 7,
            adult: 15,
          },
          reduction_days: [
            {
              days: ["jeudi"],
              reduction: 3,
              type: "substraction",
            },
          ],
        },
        {
          activity_code: 54322323,
          title: "Billet Arc de Triomphe",
          description:
            "Ordonné par Napoléon dès 1806,  l'Arc de Triomphe est une version néoclassique des arcs antiques romains dont la construction a pris près de 30 ans. Erigé en l'honneur de la Grande Armée, l'architecture du monument comprend des sculptures en relief au pied de chacun des 4 piliers, une fresque de soldats entourant le sommet ainsi que le nom des 128 batailles et 558 généraux. ",
          image_src:
            "https://media.ceetiz.com/activity/CMNPAR001/Arc-de-Triomphe---Patrick-Cadet-CMN-Paris.jpg",
          days_off: ["samedi", "dimanche"],
          reduction_days: [
            {
              days: ["mercredi", "jeudi"],
              reduction: 20,
              type: "percent",
            },
          ],
          price: 13,
        },
        {
          activity_code: 23453646,
          title: "Dîner Croisière à Paris",
          description:
            "Confortablement installé, votre maître d'hôtel, vous servira un repas gastronomique préparé à bord, accompagné de boissons et de vins. La magie opérera au son des premières notes de violon et de piano." +
            "Vous aurez  l'occasion de découvrir les incontournables de Paris illuminés : la Tour Eiffel, le Grand Palais, la Place de la Concorde, le Louvre, la Conciergerie, l'Hôtel de ville, la Bibliothèque Nationale de France, la Cathédrale Notre Dame, le Musée d'Orsay, l'Assemblée Nationale" +
            "Le temps d'une soirée, laissez vous tenter par ce dîner croisière Bateaux Parisiens !",
          image_src:
            "https://media.ceetiz.com/activity/BISPAR006/diner-croisiere-bateau-parisien-paris-ceetiz-2.jpg",
          price: 46,
          limited_reservation: 20,
        },
      ];
      commit("SET_ACTIVITIES", activities);
    },

    addToCart({ commit }, activity) {
      commit("ADD_CART_LIST", activity);
      commit("DISPLAY_ALERT", {
        mainColor: "teal",
        backgroundColor: "aliceblue",
        message: "Votre reservation a été ajoutée au panier",
        icon: "mdi-check-circle-outline",
      });
    },

    removeToCart({ commit }, code) {
      commit("REMOVE_CART_LIST", code);
      commit("DISPLAY_ALERT", {
        mainColor: "teal",
        backgroundColor: "aliceblue",
        message: "Votre reservation a été retirée du panier",
        icon: "mdi-check-circle-outline",
      });
    },
  },
});
